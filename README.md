# ADW Common Bundle
Общие вещи для наших решений

## Конфигурация
```
#!yaml
adw_common:
    doctrine_table_prefix: %my_awesome_table_prefix% # Префикс для названий таблиц
    response_headers: # Дефолтные заголовки для ответов
        X-Frame-Options: deny
        X-Content-Type-Options: nosniff
```

config.yml:
```
#!yaml
monolog:
    channels: [domain]
    handlers:
        domain:
            type:  rotating_file
            path:  "%kernel.logs_dir%/domain/%kernel.environment%.log"
            level: info
...

twig:
    form:
        resources:
            - ADWCommonBundle:Form:widgets.html.twig
```

routing.yml:
```
# adw_common
adw_common_api:
    resource: "@ADWCommonBundle/API/Controller/"
    type:     annotation
    prefix:   /api/adw/common
```

## Валидация

Включена обработка исключений для api.
У фронтов разработаны/разрабатываются решения для обработки наших [форматов](http://adw-docs.readthedocs.org/ru/latest/common/)

Выбросить ошибку валидации формы:
```
#!php

throw new ADW\CommonBundle\Exception\InvalidFormException($form);
```
Выбросить ошибку валидации с дополнительным списком ошибок
```
#!php

throw new ADW\CommonBundle\Exception\InvalidFormException($form, $myExtraConstraintViolationsList);
```
Под дополнительным списком ошибок имеются ввиду ошибки которые появляются не во время обработки формы, а например при запросе в CRM.

Пример из кода:

```
#!php

if (!$form->handleRequest($request)->isValid()) {
    throw new InvalidFormException($form);
}

try {
    $this->getUserRepository()->save($user);
} catch (ValidationViolationException $e) {
    throw new InvalidFormException($form, $e->getConstraintViolationList());
}
```

## Использование
### Autocomplete

Тип определен как сервис 'common_autocomplete'

Опции:

обязательные:

**target_entity_class** - класс сущности

обязательные:

**target_entity_id_field** - поле, в котором указан id сущности. По умолчанию **id**

**target_entity_term_field** - поле по которому осуществляется поиск соврадений. По умолчанию **title**

**allow_add** - в случае, если не выбран ни один вариант, разрешить добавление сущности с введенным в поле 
значением поля target_entity_field. По умолчанию **false**
```
#!php
->add('usr_product', 'common_autocomplete', array(
    'label'               => false,
    'target_entity_class' => UsrProduct::class,
    'target_entity_field' => 'description',
    'allow_add'           => true,
))
```

**ADW\CommonBundle\Exception\ValidationViolationException** это эксепшн который должен выбрасываться когда обнаружены ошибки в валидации чего либо. Его можно перехватить и привязать к форме как в примере выше. Если даже не будет перехвачен - будет выведен в нормальном формате.