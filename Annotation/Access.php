<?php

namespace ADW\CommonBundle\Annotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class Access.
 *
 * @author Artur Vesker
 *
 * @Annotation
 */
class Access extends Security
{
    /**
     * @return string
     */
    public function getAliasName()
    {
        return 'rest_access';
    }
}
