<?php

namespace ADW\CommonBundle\Annotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class AuthorizedOnly
 *
 * @author Artur Vesker
 * @Annotation
 */
class AuthorizedOnly extends Security
{

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        parent::__construct(array_merge(['value' => 'has_role(\'ROLE_USER\')'], $values));
    }


}