<?php

namespace ADW\CommonBundle\Serializer\Handler;

use Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\SerializationContext;
use Sonata\MediaBundle\Provider\Pool;

/**
 * Class SonataMediaHandler
 *
 * @author Artur Vesker
 * TODO: take media class name from config
 */
class SonataMediaHandler implements SubscribingHandlerInterface
{

    /**
     * @var Pool
     */
    protected $mediaPool;

    /**
     * SonataMediaHandler constructor.
     * @param Pool $mediaPool
     */
    public function __construct(Pool $mediaPool)
    {
        $this->mediaPool = $mediaPool;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'type' => Media::class,
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'method' => 'serializeMediaToJson'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function serializeMediaToJson(
        JsonSerializationVisitor $visitor,
        \Sonata\MediaBundle\Model\Media $media,
        array $type,
        SerializationContext $context
    )
    {
        return [
            'link' => $this->mediaPool->getProvider($media->getProviderName())->generatePublicUrl($media, 'reference'),
            'id' => $media->getId()
        ];
    }

}