<?php

namespace ADW\CommonBundle\Tests\Mock;

use ADW\CommonBundle\Tests\Mock\Response\FakeResponse;
use GuzzleHttp\Handler\MockHandler;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class MockResponsesTrait
 *
 * @author Artur Vesker
 */
trait MockResponsesTrait
{

    /**
     * @var string
     */
    protected $responseFixtureDir;

    /**
     * @param string $responseFixtureDir
     */
    public function setResponseFixtureDir($responseFixtureDir)
    {
        $this->responseFixtureDir = $responseFixtureDir;
    }

    /**
     * @param Client $client
     * @return MockHandler
     */
    public function getResponsesQueue(Client $client)
    {
        return $client->getContainer()->get('guzzle_mock_handler');
    }

    public function loadResponse($name, $format = 'raw')
    {
        if (!$this->responseFixtureDir) {
            throw new \Exception('Use setResponseFixtureDir() before');
        }

        return FakeResponse::load($this->responseFixtureDir . '/' . $name);
    }

}