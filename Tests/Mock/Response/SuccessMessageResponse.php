<?php

namespace ADW\AiloveBundle\Tests\Mock\Response;

use GuzzleHttp\Psr7\Response;

/**
 * Class SuccessMessageResponse.
 *
 * @author Artur Vesker
 */
class SuccessMessageResponse extends Response
{
    public function __construct()
    {
        $body = '{"status": "success", "data": {"success": "Все хорошо."}}';
        parent::__construct(200, [], $body);
    }
}
