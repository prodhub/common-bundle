<?php

namespace ADW\AiloveBundle\Tests\Mock\Response;

use GuzzleHttp\Psr7\Response;

/**
 * Class SuccessLoginResponse.
 *
 * @author Artur Vesker
 */
class LoginResponseMock extends Response
{


    public function __construct($valid = true, $createUser = false)
    {
        if ($valid) {
            $status = 200;
            $body = '{"status":"success","data":{"id":2040172,"register_date":"2015-08-19T15:04:46Z","last_login":"2015-08-19T11:26:59Z","version":1442018103,"email_verified":"not_verified","phone_verified":"not_verified","email":"proplan_test@test.ru","phone":"+79898989893","facebook_id":null,"vkontakte_id":null,"odnoklassniki_id":null,"google_id":null,"twitter_id":null,"instagram_id":null,"firstname":"кир","lastname":"киров","middlename":"кирович","gender":"none","birthday":"1980-01-01","type":{"name":"Ветеринар","system_name":"vet"},"avatar":null,"full_any_address":false,"age":35,"contacts":[{"id":78790,"fias_city":null,"fias_street":null,"fias_settlement":null,"fias_region":null,"fias_district":null,"city_type":"г","city_parent":null,"country":null,"settlement_type":null,"region":"","district":"","city":"","settlement":"","street":"","street_type":"","house":"","building":"","construction":"","apartment":"","zipcode":"","contact_type":"general","phone_mobile":"","phone_home":""}],"send_email_permission":false,"send_sms_permission":false,"vet":{"graduation_year":"1999","work_experience":0,"clinic":null,"institution":{"name":"Аграрный колледж УО \"Витебская Ордена \"Знак Почёта\" государственная академия ветеринарной медицины \"","system_name":"agrarnyi-kuvozpgavm","alt_name":""},"general_practice":false,"therapy":false,"surgery":false,"ophthalmology":false,"traumatology":false,"endocrinology":false,"dentistry":false,"inst_diag":false,"private_practice":false},"breeder":null,"token":{"expire":"1442622903","key":"1e6de1552658c30cc62f3b1b7137ca10abec093e"}}}';
        } else {
            $status = 400;

            if ($createUser) {
                $body = '{"status":"error","data":{"status_code":400,"vet":{"graduation_year":["Date has wrong format. Use one of these formats instead: YYYY."]},"error_message":null,"error_code":"validation_error"}}';
            } else {
                $body = '{"status": "error", "data": {"status_code": 400, "error_message": "Неверный email/телефон или пароль.", "error_code": "incorrect_login"}}';
            }
        }

        parent::__construct($status, [], $body);
    }
}
