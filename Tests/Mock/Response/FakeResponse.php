<?php

namespace ADW\CommonBundle\Tests\Mock\Response;

use GuzzleHttp\Psr7\Response;

/**
 * Class FakeResponse
 *
 * @author Artur Vesker
 */
class FakeResponse extends Response
{

    public static function load($file, $format = 'raw')
    {
        $message = file_get_contents($file);

        return \GuzzleHttp\Psr7\parse_response($message);
    }

    public static function create($body)
    {
        return new Response(200, [], $body);
    }

}