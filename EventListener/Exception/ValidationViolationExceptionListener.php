<?php

namespace ADW\CommonBundle\EventListener\Exception;

use ADW\CommonBundle\Exception\ValidationViolationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Class ValidationViolationExceptionListener
 *
 * @author Artur Vesker
 */
class ValidationViolationExceptionListener
{

    /**
     * {@inheritdoc}
     */
    public function onException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof ValidationViolationException) {
            return;
        }

        $violationList = $exception->getConstraintViolationList();

        $normalized = [];

        /**
         * @var ConstraintViolationInterface $violation
         */
        foreach ($violationList as $violation) {
            $normalized[] = [
                'name' => $violation->getPropertyPath(),
                'message' => $violation->getMessage()
            ];
        }

        $event->setResponse(new JsonResponse(
            [
                'code' => 'invalid_form',
                'message' => $exception->getMessage(),
                'fields' => $normalized
            ],
            400
        ));
    }

}