<?php

namespace ADW\CommonBundle\API\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/form")
 *
 * Class FormController
 * @package ADW\CommonBundle\API\Controller
 */
class FormController extends FOSRestController
{
    /**
     * @ApiDoc(
     *     section="Form",
     *     description="get entities for autocomplete (adw_common_api__form__autocomplete)",
     *     parameters={
     *      { "name"="term", "dataType"="string", "description"="Type part of term", "required"=true },
     *     }
     * )
     * @Method("GET")
     * @Route("/autocomplete", name="adw_common_api__form__autocomplete")
     * @View()
     * @param Request $request
     * @return array
     */
    public function autocompleteAction(Request $request)
    {
        $term = trim(strip_tags($request->get('term')));
        $limit = $request->get('limit');
        $targetEntityClass = urldecode($request->get('target_entity_class'));
        $targetEntityTermField = urldecode($request->get('target_entity_term_field'));
        $targetEntityIdField   = urldecode($request->get('target_entity_id_field'));

        $entities = $this->getDoctrine()->getRepository($targetEntityClass)->createQueryBuilder('e')
            ->where('e.' . $targetEntityTermField . ' LIKE :term')
            ->setParameter('term', '%'.$term.'%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $termGetter = 'get' . ucfirst($targetEntityTermField);
        $idGetter   = 'get' . ucfirst($targetEntityIdField);

        foreach($entities as &$entity) {
            $entity = array(
                'id' => $entity->$idGetter(),
                'value' => $entity->$termGetter(),
            );
        }

        return $entities;
    }
}