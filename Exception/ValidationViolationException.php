<?php

namespace ADW\CommonBundle\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ValidationViolationException.
 *
 * @author Artur Vesker
 */
class ValidationViolationException extends \RuntimeException
{

    /**
     * @var ConstraintViolationListInterface
     */
    protected $constraintViolationList;

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     * @param string $message
     * @param \Exception|null $previous
     */
    public function __construct(ConstraintViolationListInterface $constraintViolationList, $message = 'Validation Error', \Exception $previous = null)
    {
        $this->constraintViolationList = $constraintViolationList;
        parent::__construct($message, 400, $previous);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getConstraintViolationList()
    {
        return $this->constraintViolationList;
    }
}
