<?php

namespace ADW\CommonBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PublicationTrait
 *
 * @package ADW\CommonBundle\Model
 * @author Artur Vesker
 */
trait PublicationTrait
{

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $published = false;

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param boolean $published
     * @return self
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

}