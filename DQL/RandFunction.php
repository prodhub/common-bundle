<?php

namespace ADW\CommonBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class RandFunction extends FunctionNode
{
    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $name = $sqlWalker->getConnection()->getDatabasePlatform()->getName();
        if ($name == 'mysql') {
            return 'RAND()';
        }
        if ($name == 'sqlite') {
            return 'RANDOM()';
        }

        throw new \InvalidArgumentException(sprintf('Unexpected database platform "%s"', $name));
    }

    /**
     * @param Parser $parser
     *
     * @return void
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}