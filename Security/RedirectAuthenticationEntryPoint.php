<?php

namespace ADW\CommonBundle\Security;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

/**
 * Class RedirectAuthenticationEntryPoint
 *
 * @package ADW\CommonBundle\Security
 * @author Artur Vesker
 */
class RedirectAuthenticationEntryPoint implements AuthenticationEntryPointInterface
{

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    protected $route;

    /**
     * @var array
     */
    protected $routeParams;

    /**
     * @param Router $router
     * @param string $route
     * @param array $routeParams
     */
    public function __construct(Router $router, $route, array $routeParams = [])
    {
        $this->router = $router;
        $this->route = $route;
        $this->routeParams = $routeParams;
    }

    /**
     * @inheritdoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate($this->route, $this->routeParams));
    }

}