<?php

namespace ADW\CommonBundle\ApiDoc;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Nelmio\ApiDocBundle\Extractor\HandlerInterface;
use Symfony\Component\Routing\Route;

/**
 * Class RouteHandler.
 *
 * @author Artur Vesker
 */
class RouteHandler implements HandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(ApiDoc $annotation, array $annotations, Route $route, \ReflectionMethod $method)
    {
        if (!$annotation instanceof \ADW\CommonBundle\Annotation\ApiDoc) {
            return;
        }

        foreach ($annotations as $annot) {
            if ($annot instanceof \Sensio\Bundle\FrameworkExtraBundle\Configuration\Route) {
                $annotation->setRouteName($annot->getName());

                return;
            }
        }
    }
}
