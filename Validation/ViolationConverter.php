<?php

namespace ADW\CommonBundle\Validation;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Class ViolationConverter
 *
 * @package ADW\CommonBundle\Validation
 * @author Artur Vesker
 *
 * //TODO: Maybe need optimization
 */
class ViolationConverter
{

    /**
     * @param ConstraintViolationInterface $violation
     * @param FormInterface $form
     * @return ConstraintViolation
     */
    public function toFormFormat(ConstraintViolationInterface $violation, FormInterface $form)
    {
        $parts = $this->explodePath($violation->getPropertyPath());

        foreach ($parts as $key => $part) {

            $form = $this->findForm($part, $form);

            if (!$form) {
                $parts = [];
                break;
            }

            $part = $form->getName();

            $parts[$key] = "children[$part]";
        }

        return new ConstraintViolation(
                $violation->getMessage(),
                $violation->getMessageTemplate(),
                $violation->getMessageParameters(),
                $violation->getRoot(),
                implode('.', $parts),
                $violation->getInvalidValue()
            );
    }

    /**
     * @param $path
     * @return array
     */
    protected function explodePath($path)
    {
        return explode('.', str_replace('[', '.', str_replace(']', null, $path)));
    }

    /**
     * @param $fieldName
     * @param FormInterface $parent
     * @return mixed|null|FormInterface
     */
    protected function findForm($fieldName, FormInterface $parent)
    {
        if (is_numeric($fieldName) && $parent->has((int)$fieldName)) {
            return $parent[(int)$fieldName];
        }

        //Try find by original name
        if ($field = $this->findByName($fieldName, $parent)) {
            return $field;
        }

        $camelCaseToSnakeCaseConverter = new CamelCaseToSnakeCaseNameConverter();

        //Try find by snake case name
        if ($field = $this->findByName($camelCaseToSnakeCaseConverter->normalize($fieldName), $parent)) {
            return $field;
        }

        // Okay... try find by defined custom property paths
        foreach ($parent as $form) {
            if (!($customPath = $form->getConfig()->getPropertyPath())) {
                continue;
            }

            if ((string)$customPath == $fieldName) {
                return $form;
            }
        }

        return null;
    }

    /**
     * @param $fieldName
     * @param FormInterface $parent
     * @return null|FormInterface
     */
    protected function findByName($fieldName, FormInterface $parent)
    {
        if ($parent->has($fieldName)) {
            $byName = $parent->get($fieldName);
            if (!($customPath = $byName->getConfig()->getPropertyPath())) {
                return $byName;
            }

            if ((string)$customPath == $fieldName) {
                return $byName;
            }
        }

        return null;
    }


}